const mongoose = require('mongoose')
const { Schema } = mongoose
const productSchema = Schema({
  product_name: String,
  category: String,
  price: Number,
  minimum_bid_price: Number,
  Auction_start_time: String,
  Auction_crossing_time: String,
  Auction_start_date: String,
  Auction_crossing_date: String,
  imagePath: String,
  status: String,
  product_details: String
})
module.exports = mongoose.model('Product', productSchema)
