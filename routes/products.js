const express = require('express')
const router = express.Router()
const Product = require('../models/Product')

const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
// GET One Product
const getProduct = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Prduct not found!!'
      })
    }
    res.json(product)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}
// Add New Product
const addProducts = async function (req, res, next) {
  const newProduct = new Product({
    product_name: req.body.product_name,
    category: req.body.category,
    price: parseFloat(req.body.price),
    minimum_bid_price: parseFloat(req.body.minimum_bid_price),
    Auction_start_time: req.body.Auction_start_time,
    Auction_crossing_time: req.body.Auction_crossing_time,
    Auction_start_date: req.body.Auction_start_date,
    Auction_crossing_date: req.body.Auction_crossing_date,
    imagePath: req.body.imagePath,
    status: req.body.status,
    product_details: req.body.product_details
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const updateProducts = async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.product_name = req.body.product_name
    product.category = req.body.category
    product.price = parseFloat(req.body.price)
    product.minimum_bid_price = parseFloat(req.body.minimum_bid_price)
    product.Auction_start_time = req.body.Auction_start_time
    product.Auction_crossing_time = req.body.Auction_crossing_time
    product.Auction_start_date = req.body.Auction_start_date
    product.Auction_crossing_date = req.body.Auction_crossing_date
    product.imagePath = req.body.imagePath
    product.status = req.body.status
    product.product_details = req.body.product_details
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
const deleteProducts = async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(400).send({ message: err.message })
  }
}
router.get('/', getProducts) // GET Products
router.get('/:id', getProduct) // GET One Product
router.post('/', addProducts) // Add New Product
router.put('/:id', updateProducts) // UPDATE Product
router.delete('/:id', deleteProducts) // DELETE Product

module.exports = router
