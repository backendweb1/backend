const mongoose = require('mongoose')
const Product = require('../models/Product')
mongoose.connect('mongodb://localhost:27017/example')
async function clearProduct () {
  await Product.deleteMany({})
}
async function main () {
  await clearProduct()
  for (let i = 0; i < 12; i++) {
    const product = new Product({
      product_name: 'Product1' + i,
      category: 'Category1',
      price: 1000,
      minimum_bid_price: 10,
      Auction_start_time: '110000',
      Auction_crossing_time: ' 110000',
      Auction_start_date: '20230101',
      Auction_crossing_date: '20230102',
      imagePath: null,
      status: 'เปิด',
      product_details: 'ไม่มีอะไรเลยนอกจากเงินเทอจะชอบมั้ยน้า'
    })
    product.save()
  }
}
main().then(function () {
  console.log('Finish')
})
